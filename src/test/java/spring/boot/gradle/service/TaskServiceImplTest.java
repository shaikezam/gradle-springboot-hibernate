package spring.boot.gradle.service;

/*
 * import static org.junit.Assert.assertEquals; import static
 * org.junit.Assert.assertNull;
 * 
 * import org.junit.Before; import org.junit.Ignore; import
 * org.junit.runner.RunWith; import
 * org.springframework.beans.factory.annotation.Autowired; import
 * org.springframework.boot.context.embedded.LocalServerPort; import
 * org.springframework.boot.test.context.SpringBootTest; import
 * org.springframework.boot.test.context.SpringBootTest.WebEnvironment; import
 * org.springframework.test.context.TestPropertySource; import
 * org.springframework.test.context.junit4.SpringRunner;
 * 
 * import spring.boot.gradle.model.Task;
 * 
 * @RunWith(SpringRunner.class)
 * 
 * @SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
 * 
 * @TestPropertySource(locations = "classpath:test.properties") public class
 * TaskServiceImplTest {
 * 
 * @LocalServerPort int randomServerPort;
 * 
 * @Autowired private TaskService taskService; private Task task = null;
 * 
 * @Before public void init() { task = new Task("1st task", "Hello World");
 * taskService.addTask(task); }
 * 
 * @Ignore public void testCreateTaskSuccess() { assertEquals("1st task",
 * taskService.getTask("1").getTitle()); }
 * 
 * @Ignore public void testUpdateTaskSuccess() { task = new Task("2st task",
 * "Hello World"); task.setId(1); taskService.updateTask("1", task);
 * assertEquals("2st task", taskService.getTask("1").getTitle()); }
 * 
 * @Ignore public void testDeleteTaskSuccess() { taskService.deleteTask("1");
 * assertNull(taskService.getTask("1")); } }
 */
