package spring.boot.gradle.repository;

import org.springframework.data.repository.CrudRepository;

import spring.boot.gradle.model.Task;

public interface TaskRepository extends CrudRepository<Task, Integer> {
}
