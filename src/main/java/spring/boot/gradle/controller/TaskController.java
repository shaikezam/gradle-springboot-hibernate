package spring.boot.gradle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spring.boot.gradle.model.Task;
import spring.boot.gradle.service.TaskService;

@RestController
public class TaskController {

	private TaskService taskService;

	@Autowired
	public TaskController(TaskService taskService) {
		this.taskService = taskService;
	}

	@GetMapping("/tasks")
	public List<Task> getAllTasks() {
		return taskService.getTasks();
	}

	@PostMapping(value = "/tasks")
	public int createTask(@RequestBody Task task) {
		return taskService.addTask(task);//
	}

	@GetMapping("/tasks/{id}")
	public Task getTaskByID(@PathVariable String id) {
		return taskService.getTask(id);
	}

	@PutMapping("/tasks/{id}")
	public String updateTaskByID(@PathVariable String id, @RequestBody Task task) {
		return taskService.updateTask(id, task);
	}

	@DeleteMapping("/tasks/{id}")
	public String deleteTaskByID(@PathVariable String id) {
		return taskService.deleteTask(id);
	}

}
