package spring.boot.gradle.service;

import java.util.List;

import spring.boot.gradle.model.Task;

public interface TaskService {
	public List<Task> getTasks();

	public Task getTask(String id);

	public int addTask(Task task);

	public String updateTask(String id, Task task);

	public String deleteTask(String id);
}
