package spring.boot.gradle.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.boot.gradle.model.Task;
import spring.boot.gradle.repository.TaskRepository;

@Service
public class TaskServiceImpl implements TaskService {

	private TaskRepository taskRepository;

	@Autowired
	public TaskServiceImpl(TaskRepository taskRepository) {
		this.taskRepository = taskRepository;
	}

	@Override
	public List<Task> getTasks() {
		List<Task> returnTasks = new ArrayList<>();
		taskRepository.findAll().forEach(returnTasks::add);
		return returnTasks;
	}

	@Override
	public Task getTask(String id) {
		return taskRepository.findOne(Integer.parseInt(id));
	}

	@Override
	public int addTask(Task task) {
		taskRepository.save(task);
		return task.getId();
	}

	@Override
	public String updateTask(String id, Task task) {
		taskRepository.save(task);
		return id;
	}

	@Override
	public String deleteTask(String id) {
		taskRepository.delete(Integer.parseInt(id));
		return id;
	}

}
